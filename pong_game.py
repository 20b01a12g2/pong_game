
import turtle

from pygame import mixer


mixer.init()
mixer.music.load('C:\\Users\\admin\\Downloads\\mixkit-magical-intro-presentation-1147.wav')
mixer.music.play()

a_wins = 0
b_wins = 0
w = turtle.Screen()
w.title(" Pong game ")
w.bgcolor("black")
w.setup(width=800, height=600)
w.tracer(0)

score_a = 0
score_b = 0
score_limtit = 11

paddle_a = turtle.Turtle()
paddle_a.speed()
paddle_a.color("white")
paddle_a.shape("square")
paddle_a.penup()
paddle_a.goto(-350, 0)
paddle_a.shapesize(stretch_wid=5, stretch_len=1)

paddle_b = turtle.Turtle()
paddle_b.speed()
paddle_b.color("white")
paddle_b.shape("square")
paddle_b.penup()
paddle_b.goto(350, 0)
paddle_b.shapesize(stretch_wid=5, stretch_len=1)

paddle_a_speed = 60
paddle_b_speed = 60

ball = turtle.Turtle()
ball.speed()
ball.shape("circle")
ball.color("red")
ball.goto(0, 0)
ball.penup()
ball.dy = 0.9
ball.dx = 0.9

pen = turtle.Turtle()
pen.speed()
pen.penup()
pen.color("white")
pen.hideturtle()
pen.goto(0, 240)
pen.write("Player A: 0  Player B: 0", align="center", font=("Arial", 24, "italic"))

win = turtle.Turtle()
win.speed()
win.penup()
win.color("white")
win.hideturtle()
win.goto(0, 0)

def paddle_a_up():
    y = paddle_a.ycor()
    if (y < 220):
        y += paddle_a_speed
        paddle_a.sety(y)
    


def paddle_a_down():
    y = paddle_a.ycor()
    if (y > -220):
        y -= paddle_a_speed
        paddle_a.sety(y)
    


def paddle_b_up():
    y = paddle_b.ycor()
    if (y < 220):
        y += paddle_b_speed
        paddle_b.sety(y)
    


def paddle_b_down():
    y = paddle_b.ycor()
    if (y > -220):
        y -= paddle_b_speed
        paddle_b.sety(y)

turtle.listen()
turtle.onkey(paddle_a_up, "a")
turtle.onkey(paddle_a_down, "z")
turtle.onkey(paddle_b_up, "Up")
turtle.onkey(paddle_b_down, "Down")

while 1:
    w.update()
    ball.setx(ball.xcor() + ball.dx)
    ball.sety(ball.ycor() + ball.dy)
    if ball.ycor() > 220:
        ball.sety(220)
        ball.dy *= -1
    elif ball.ycor() < -220:
        ball.sety(-220)
        ball.dy *= -1
    elif ball.xcor() > 390:
        mixer.music.load('C:\\Users\\admin\\Downloads\\mixkit-losing-bleeps-2026.wav')
        mixer.music.play()
        ball.goto(0, 0)
        ball.dx *= -1
        score_a += 1
    elif ball.xcor() < -390:
        mixer.music.load('C:\\Users\\admin\\Downloads\\mixkit-losing-bleeps-2026.wav')
        mixer.music.play()
        ball.goto(0, 0)
        ball.dx *= -1
        score_b += 1

    if 340 < ball.xcor() < 350 and paddle_b.ycor() + 50 > ball.ycor() >= paddle_b.ycor() - 50:
        mixer.music.load('C:\\Users\\admin\\Downloads\\mixkit-game-ball-tap-2073.wav')
        mixer.music.play()
        ball.setx(340)
        ball.dx *= -1

        
    if -340 > ball.xcor() > -350 and paddle_a.ycor() + 50 > ball.ycor() >= paddle_a.ycor() - 50:
        mixer.music.load('C:\\Users\\admin\\Downloads\\mixkit-game-ball-tap-2073.wav')
        mixer.music.play()
        ball.setx(-340)
        ball.dx *= -1
    pen.clear()
    pen.write(f"Player A: {score_a}       Player B: {score_b}", align="center", font=("Arial", 26, "bold"))
    if score_a == score_limtit:
        turtle.clearscreen()
        a_winning = True
        break
    elif score_b == score_limtit:
        turtle.clearscreen()
        b_wiining = True
        break

while 1:

    if (a_wins == 1):

        w.bgcolor("black")
        win.write("Player A wins", align="center", font=("Arial", 50, "bold"))


    elif (b_wins == 1):

        w.bgcolor("black")
        win.write("Player B wins", align="center", font=("Arial", 50, "bold"))






